Rails.application.routes.draw do

  get 'tasks/list', to: "tasks#user_tasks"
  get 'user/login_show', to: "users#login_page"
  post 'tasks/complete', to: "tasks#task_completed", as: 'update_tasks' , :defaults => { :format => 'json' }

  get 'users/login', to: "users#login" , :defaults => { :format => 'xml' }
  get 'tasks/all_tasks', to: "tasks#all_tasks"
  get 'tasks/task_user', to: "tasks#task_user"
  get 'tasks/score_board', to: "tasks#score_board"

  resources :users
  resources :tasks
  root to: "users#home"

end
