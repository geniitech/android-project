set :stage, :staging

server 'geniilabs.in', user: 'webadmin', roles: %w{web app db}, runner: "webadmin", password: "Qwerty123!"

set :deploy_to, "/home/webadmin/sites/android-project"
set :user, 'webadmin'
set :rails_env, 'staging'