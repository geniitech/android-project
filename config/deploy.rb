#################### Don't use sudo unless really necessary ##################

# config valid only for Capistrano 3.2
lock '3.2.1'
set :application, 'android-project'
set :repo_url, 'git@bitbucket.org:geniitech/android-project.git'
set :stages, %w(production staging)
set :branch, ENV["REVISION"] || ENV["BRANCH_NAME"] || "master"
set :scm, :git
set :log_level, :debug
set :keep_releases, 5
set :ssh_options, {
  forward_agent: true
}

set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/assets}
set :default_stage, 'staging'

set :assets_dir, "public/system"
set :local_assets_dir, "public"

# configuration for rvm
# set :rvm_type, :user                     # Defaults to: :auto
# set :rvm_ruby_version, 'ruby-1.9.3-p484'      # Defaults to: 'default'
# set :rvm_custom_path, '~/.myveryownrvm'  # only needed if not detected

# configuration for interactive sessions
# set :default_run_options, {
#   pty: true
# }
# set :use_sudo, true

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

namespace :deploy do
  desc 'Restart application'
  task :restart do
    invoke "passenger:restart"
  end

  after :publishing, :migrate
  after :publishing, :restart
end

namespace :setup do
  desc "Create deploy directory"
  task :create_directory do
    on roles(:app) do
      execute :mkdir, '-p', deploy_to
      execute :chown, "#{fetch(:user)}", deploy_to
    end
  end

  desc "Create database and database user"
  task :create_mysql_database do
    ask :db_name, fetch(:db_name)
    ask :db_user, ''
    ask :db_pass, ''

    on roles(:app), wait: 5 do
      execute "mysql --user=#{fetch(:db_user)} --password=#{fetch(:db_pass)} -e \"CREATE DATABASE IF NOT EXISTS #{fetch(:db_name)}\""
    end
  end
end

# Custom tasks

namespace :passenger do
  desc "Restart rails application"
  task :restart do
    on roles(:app), wait: 5 do
      execute :touch, current_path.join('tmp/restart.txt')
    end
  end
end

namespace :thin do
  desc "Restart thin server"
  task :restart do
    on roles(:app), wait: 5 do
      execute "thin restart -C /etc/thin/shmart.yml"
    end
  end
end

namespace :logs do
  desc "tail rails logs"
  task :tail do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/#{fetch(:stage)}.log"
    end
  end
end