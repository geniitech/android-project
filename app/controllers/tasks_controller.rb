class TasksController < ApplicationController
  # before_filter :auth_valid_token
  
  def all_tasks
    @tasks = Task.find_by status: true
    respond_to do |format|
      format.html { render template: 'task_user' }
      format.json { render json: @tasks }
      format.xml { render xml: @tasks }
    end
  end

  def user_pending
    user = get_user
    @tasks = TaskUser.where("created_at > ? AND user_id = ?", Time.now-24.hours, user.id).pluck(:id)
    @pending_tasks = Task.where('id not in (?) and status = 1', @tasks)
    respond_to do |format|
      format.json { render json: @pending_tasks }
      format.xml { render xml: @pending_tasks }
    end
  end

  def task_completed
    user = get_user    
    task = user.tasks.find(params[:task_id])
    user.points = user.points.to_i + task.points.to_i
    user.save!
    task.status = !task.status
    task.save!
    respond_to do |format|
      format.json { render json: {status: true} }
    end    
  end

  def score_board
    @users = User.all
    @users.each do |user|
      user.points = user.tasks.sum(:points)
      user.save!
    end
  end

  def user_tasks
    user = get_user
    @tasks = user.tasks.where(:status => true)
    render :task_user
  end

  def show
    @users = User.all
    @users.each do |user|
      user.points = user.tasks.sum(:points)
    end
    @users = @users.sort_by{ |hsh| hsh[:points] }.reverse


    # email = params[:email]
    # user = User.find_by email: email
    # if user
    #   @tasks = TaskUser.where("created_at > ?", Time.now-24.hours)
    # else
    #   @tasks = {status => "500"}
    # end
    # # respond_to do |format|
    # #   format.json { render json: @tasks }
    # #   format.xml { render xml: @tasks }
    # # end
  end
  
  # We don't need this method because points are updated on completion of task

  # def add_points
  #   user = get_user
  #   new_points = params[:points]
  #   user.points = user.points.to_i + new_points.to_i
  #   user.save!
  # end

  def auth_valid_token
    auth_token = params[:token]
    user = User.where(:auth_token => auth_token).first
    if user
      return true
    else
      return false
    end

  end

  def get_user
    auth_token = params[:token]
    user = User.where(:auth_token => auth_token).first
  end

end