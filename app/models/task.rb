class Task < ActiveRecord::Base
  has_many :users, through: :task_users
  has_many :task_users

  # def self.score_board
  #   @users = User.all
  #   @users.each do |user|
  #     user.points = user.tasks.sum(:points)
  #     user.save!
  #     @users = User.all.collect{ |x| x.tasks.sum(:points) }
  #   end
  # end

end
