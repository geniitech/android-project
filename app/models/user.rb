class User < ActiveRecord::Base
  has_many :task_users
  # association between users and tasks
  has_many :tasks, through: :task_users
end
